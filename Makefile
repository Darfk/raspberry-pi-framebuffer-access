xCC = gcc

CFLAGS = -g -Wall

INCLUDE = -I. -I/usr/include/

#no libs
# LDFLAGS =

#no libs
# LDLIBS = 

SRCS = main.c

OBJS = main.o

.c.o: $(SRCS)
	$(CC) $(DEFS) $(INCLUDE) $(CFLAGS) -c $<

all: main

main: $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDE) $(OBJS) $(LDFLAGS) $(LDLIBS) -o $@

run: 
	./main

clean:
	rm -f *.o main
